FROM registry.access.redhat.com/ubi8/python-39

LABEL maintainer="Stenio Filho <nuvem@usp.br>"

LABEL io.k8s.description="Hello Wolrd usando python Gunicorn" \
      io.k8s.display-name="stenio-01-builder 0.0.1" \
      io.openshift.expose-services="8000:http" \
      io.openshift.tags="stenio-01-builder,0.0.1,python,gunicorn"


USER 0
COPY .s2i/bin/ /usr/libexec/s2i
ADD src /tmp/src
RUN /usr/bin/fix-permissions /tmp/src
USER 1001

EXPOSE 8000

RUN /usr/libexec/s2i/assemble

CMD /usr/libexec/s2i/run